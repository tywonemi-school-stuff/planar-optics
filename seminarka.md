---
title: "Integrovaná planární optika pro komunikační techniku"
author: Emil Jiří Tywoniak, semestrální práce OTT ZS 20/21 @ FEL ČVUT
date: 2020-01-10
geometry: "left=3cm,right=3cm,top=2cm,bottom=3cm"
output: pdf_document
---

Tato semestrální práce krátce pojednává o optických prvcích pro komunikační techniku, vhodných pro integraci do výrobních nodů pro procesory s vysokou hustotou CMOS tranzistorů (Silicon-on-Insulator), případně na separátní substráty krátkými spoji propojené s CMOS čipem.

### Vlnovody
Index lomu fosfidu inditého (InP) je přibližně 3.2 při 1550 nm a dopováním InGaAs lze zvýšit až na přibližně 3.5 [@Fiedler1987]. Ztráty InP vlnovodu jsou v rozsahu 0.4 až 6 dB/cm, podle geometrie a polarizace [@DAgostino2015]. Pro srovnání, pro stejnou vlnovou délku je index lomu křemíku 3.48 a index lomu oxidu křemičitého (SiO2) je 1.44. Na 1550nm má SiO2 extinkční koeficient 0, InP 0.3. Vedení ve vlnovodech s InP substrátem má při dané geometrii větší ztráty než Si/SiO2 kvůli malému rozdílu koeficientu lomu mezi "jádrem" InGaAs/InGaAsP a "claddingem" InP. Proto pro 1550 nm má typicky vlnovod s InP substrátem rozměry 2000x500 nm, s křemíkovým substrátem jen 400x300nm. Výhodou InP ale je jednoduchost integrace s fotodiodami a zesilovači. Ty se totiž už konvenčně vyrábějí jako HEMT na InP substrátech, i v na vlákno vázaných aplikacích. InP zesilovače jsou schopné pracovat v rozsahu stovek GHz již mnoho let [@Leong2017]. InP má totiž dobré vlastnosti co se týče životnosti optických fononů, a má pětinásobnou mobilitu elektronů oproti křemíku.

Kromě běžných mikrovlnovodů je dostupné k vedení vlnění používat i fotonické krystaly, tedy struktury s periodickým indexem lomu, v nichž se používaná vlnová délka nachází v zakázaném pásu. Pokud do takového materiálu dáme defekt, ve kterém se používaná vlnová délka šířit smí, dostaneme vlnovod s některými zajímavými vlastnostmi, jako jsou menší ztráty v ostrých ohybech, nižší grupové rychlosti, a případně pozorovatelnější nelineární jevy způsobené více lokalizovaným polem šířícíh se vidů.

### Zdroje a detektory
Typická CMOS technologie není sama o sobě vhodná pro tvorbu LED. Pro integraci se LED můžou vyrobit v jiném procesu, na CMOS wafer se pak přilepí a přibondují. Toto neumožňuje jednoduchou vazbu do mikrovlnovodu. LED může tedy být:

+ separátně od čipu s vlnovody, tedy separátní InP čip, a svítit do něj z boku - vazba je problematická
+ realizována jako vertikálně stohované vrstvy ("nanolaser") a svítit do vlnovodu skrz cladding - vazba je problematická, zlepšuje se taperem [@Roelkens2015]
+ realizována jako nanolaser a být součástí vrchního claddingu

Jedná se o netriviální technologickou a geometrickou úlohu. Účinnost může být zvýšena pokovením hran LED které nemají emitovat. Další problém integrace fotoniky s elektronikou je nutnost přítomnosti kovových propojek na vrchní hraně CMOS procesů. Hybridní řešení (tj. obě technologie jsou použity) mají složitější výrobu kvůli kompletaci dvou čipů nebo heterogenní epitaxi [@Kum2019]. Lze integrovat fotoniku i s elektronikou jiných než Si substrátů, například SiC [@Takenaka17].

Laserové diody se v měřící technice běžně konstruují planárně. Aktivní prostředí i vhodně geometricky řešený rezonátor tedy nepotřebují velké úpravy, mimo ty zmíněné pro LED. U vysoce integrované fotoniky může být ale rezonátor obrovský (řádově milimetry dlouhý, [@HongjunCao2005]) vůči ostatním prvkům, které mívají délku řádově desítek mikrometrů.

### Zpoždení
Výzkum zpožďování RF signálu planární optikou je historicky motivován beamformingem, tj. tvarováním vyzářeného pole fázovaným polem antén. Pro současné systémy zpracování optických signálů je potřeba ale zpožďovat značně širší pásmo. Zpoždění lze realizovat několika metodami:

+ [@McKay2020] realizuje zpoždění ve stovkách pikosekund na šířce pásma 1 GHz, realizované fázovým posuvem způsobený stimulovaným Brillouinovým rozptylem, dále korigovaným mikrorezonátorem
+ [@Povinelli2005] předpovídá zpoždění 3 ps na šířce pásma stovek GHz, snížením grupové rychlosti taperovanou 1D periodickou strukturou dlouhou 2 mm
+ [@Fakharzadeh2006] realizuje dvojici zpoždění, větší z nichž je 3.3ps na (ekvivalentní) šířce pásma 14 THz, na 2D fotonickém krystalu o délce hrany 25 $\mu$m a nízkou grupovou rychlostí

[@Zhang2018] pak realizuje nespecifkované absolutní zpoždění na lokalizovaným přeladěním chirpované Braggově mřížce. Tento mechanismus může ale realizovat i posuv fáze, filtraci, a má integrovanou aktivní odrazivou dutinu. Mechanismus má délku 1.56 mm a je realizovaný na substrátu kompatibilní s CMOS technologií.

### Filtrace
Obdobně jako ve vláknové optice lze z optického signálu filtrovat úzké pásmo odrazem od Braggovy mřížky vytvořené periodicky se měnícím indexem lomu. Změnou geometrie mechanickým napětím způsobeným elektrostrikcí nebo ohřevem můžeme přeladit odráženou vlnovou délku. Braggova mřížka není ale vhodná pro filtrování přimodulovaných RF signálů. Proto se pro zpracování optických signálů používají jiné techniky, často diskrétní operace (FIR filtrace) na spojitém signálu. V [@Huang2012] lze nastavit skalární koeficienty filtrace na pozitivní i negativní čísla skrze ladění koeficientů konverze fázové modulace na amplitudovou (intenzitovou) a demonstrován je frekvenčně plochý bandpass na 18.5 GHz s 11 koeficienty s jednotkovým zpožděním 386ps.

### Polarizace
Konvenční smyčkový polarizér z vláknové optiky by byl na čipu nerealizovatelný kvůli potřebnému mechanickému pnutí a rozměrům. Lze ale využít fotonické krystaly, které na rozmezí odpovídající používané vlnové délce mají velký odstup transmise TM a TE vidů. Například odstup polarizace PER v realizaci [@Ma2010] je 16 dB na rozpětí vlnových délek >50nm, polarizér má délku 20 $\mu$m.

### Modulace
V roce 1999 bylo elektrostrikcí dosaženo modulace 1MHz [@DEPatent]. Teoretický limit elektrostrikce jednovidového optického vlákna omezuje šířku pásma na stovky MHz [@Buckland1997].

Elektro optické modulátory dosahují až 500GHz šířka pásma. Mají na něm ale velké ztráty, protože potřebují vlnovody o délce stovek $\mu$m. Například @Mercante18 dosahuje modulace -25dBm na 50GHz, -40dBm na 500GHz. Zajímavý je i cíl snižovat budící napětí pro nižší spotřebu a jednodušší integraci. S budícím napětím 1V dosahuje @Brosi2008 šířky pásma 78GHz na délce 80 $\mu$m se ztrátami v jednotkách dB.

Plazmonické modulátory využívají povrchových plazmonových polaritonů, tedy interakcí dvou psuedočástic, které se dají vybudit elektromagnetickým buzením na rozhraní dielektrika s kovem. Vynikají i spínací spotřebou pouhých 2.8 fJ/bit. Touto metodou bylo dosaženo šířky pásma až 500 GHz modulátorem o délce jen 25 $\mu$m se zachováním výborné linearity a konzistentní amplitudy napříč spektrem [@burla2018500].

Akusto optické modulátory jsou silně frekvenčně omezené kvůli intrinzickým ztrátám mechanického vlnění. Mají ale potenciál být využity pro nové metody ve zpracování signálů, včetně kvantové optomechaniky, díky interakcím fononů a fotonů [@Li2015].

Všechny tyto typu modulátorů mají útlum optického signálu v jednotkách dB, zespoda omezený vlastními ztrátami vlnovodu.

### Vazba
Vazba mezi dvěma mikrovlnovody je obdobně realizovatelná, jako v mikrovlných obvodech. Pásmo vazby lze zúžit propagací vazebního pole Braggovou mřížkou nebo fotonickým krystalem [@6190714].
Účinnost vazby z mikrovlnovodu do fotonického krystalu je nízká pro manipulaci rozložení pole parabolickým zrcadlem, pro hustší integraci může ale být vhodnější coupler realizovaný fotonickým krystalem s prostorově proměnlivými parametry geometrie [@5032283]. Pro vazbu do běžného optického vlákna může být výhodné použít mezivlákno [@waveguideSMF]

## Závěr
Plně integrované opticko elektronické komunikační prvky (tj. routery, switche a NICs) se stávají realitou pro datacentra. Jedná se o pole nové, vzhledem k velkým posunům v realizovaných kvantitativních výsledcích studií vzniklých za posledních dvacet let.

## Reference


<!-- ## Vědecká optika

FOG

### Lokálně hřáté čočky

[@Berto2019]

### Metamateriálové čočky

### Korekce aberace
Důležité pro zvýšení přesnosti transmisní elektronové mikroskopie - Springer Handbook of Microscopy:
dva druhy aberace
+ chromatická - materiálem
+ sferická - geometrií, jen u bodového zdoje. Minimum poloměru "fleku" závislé na třetím řádu maximálního úhlu pod kterým bodový zdroj svítí na čočku
korektory posunuly TEM pod jeden angstrom  -->
