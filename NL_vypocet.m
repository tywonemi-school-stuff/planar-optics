% clc;
close all

%%%%% Gamma and n2 calculation %%%%%

% P0 is the average power of the input signals measured at the FUT end, 
% P1 is the average power of the idler signals measured at the FUT end,
% Pk is the average power of the input signals measured at the FUT input.



%% 20.2.2017

%P0db = [7.01	7.36	7.87	8.41	8.93	9.54	10.22	10.93	11.37	11.82	12.34	12.88	13.34	14.02	14.48	14.76	15.07];
% P1db = [-24.73	-23.50	-22.14	-20.51	-18.93	-17.15	-15.08	-12.97	-11.62	-10.28	-8.69	-7.04	-5.67	-3.59	-2.18	-1.29	-0.30];
% Pk   = [10.72	11.12	11.62	12.12	12.72	13.32	14.02	14.72	15.12	15.62	16.12	16.72	17.22	17.92	18.42	18.72	19.12];

%% 20.2.2017 nekalibrovane
% 
% P0db = [-12.81	-12.46	-11.95	-11.41	-10.89	-10.28	-9.60	-8.89	-8.45	-8.00	-7.48	-6.94	-6.48	-5.80	-5.34	-5.06	-4.75];
% P1db = [-44.55	-43.31	-41.95	-40.32	-38.74	-36.96	-34.90	-32.79	-31.44	-30.09	-28.51	-26.85	-25.49	-23.41	-21.99	-21.11	-20.12];
% Pk   = [-20.00	-19.60	-19.10	-18.60	-18.00	-17.40	-16.70	-16.00	-15.60	-15.10	-14.60	-14.00	-13.50	-12.80	-12.30	-12.00	-11.60];


%% Parameters - SET THESE

Aeff            = 11.7e-12;                                  %% Effective area in [m]
lam0            = 1.55132e-6;                                %% Central lambda between the pump signals in [m]
alfa_dB_na_km   = 0.44;                                   %% Specific attenuation [dB/km]
L               = 500;                                       %% Phisical fiber length [m]

%% Linear power 

I0   = (10.^(P0db/10).*0.001)';                              %% [W]
I1   = (10.^(P1db/10).*0.001)';                              %% [W]   
Pin  = (10.^(Pk./10).*0.001)';                               %% [W]
% Pin1 = Pin;

%% FUT attenuation and Leff

% alfa            = alfa_dB_na_km* 0.115129255 * 2 /1000; % _Np/m
% Leff            = (1-exp(-alfa*L))/alfa;
Leff            = (1-exp(-(alfa_dB_na_km*1e-3)*L))/(alfa_dB_na_km*1e-3);

%% Bessel function
phi = 0.01:0.01:2*pi;
pomer =((besselj(0,phi/2).^2 + (besselj(1,phi/2)).^2))./((besselj(1,phi/2).^2) + (besselj(2,phi/2).^2));
phi_fit         = spline(pomer,phi, I0./I1);

%% Figures

figure(1)                                   %% calculation of slope parameter (linear fit)
hold on
f = fit(Pin, phi_fit, 'poly1' );        
plot(f, Pin, phi_fit)
xlabel('FUT Input [W]');
ylabel('phi [rad]');
legend('Location', 'northwest')
 
figure(2)
hold on
plot(Pin, I0./I1)
scatter (Pin, I0./I1,'x')
xlabel('FUT Input [W]');
ylabel('I1/I0');
hold off

%% calculations

%%%%%%%%%%%%%%%%%%%% v1 %%%%%%%%%%%%%%%%%%
IL_n2_v1 = ((lam0/(4*pi*Leff))*f.p1*Aeff);                  
gamma1 = ((IL_n2_v1*2*pi)/(lam0*Aeff))*1000;                     %% *1000 because [1/Wkm]

%%%%%%%%%%%%%%%%%%%% v2 %%%%%%%%%%%%%%%%%%
IL_n2_v2 = mean((phi_fit./(Leff.*Pin*4*pi))*Aeff*lam0);         
gamma2 = ((IL_n2_v2*2*pi)/(lam0*Aeff))*1000;                     %% *1000 because [1/Wkm]

%% output

fprintf('gamma v1 = %f [1/W*km], n2 v1= %e [m2/W] \n ',gamma1,IL_n2_v1);
fprintf('gamma v2 = %f [1/W*km], n2 v2= %e [m2/W] \n',gamma2,IL_n2_v2);
